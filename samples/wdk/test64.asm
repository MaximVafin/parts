.data

; all data variables in your asm code goes here
myData1   dq   0   ; 64 bit data


.code

; all assembly routines go here

TestFunction PROC

    ; sample function/routine/procedure

    ; assembly code for the function goes here

    ret 
    
TestFunction ENDP

END ; end of assembly file
